package main

import (
	"os"
	"log"
	"time"
	"bytes"
	"strings"
	"os/exec"
	"net/http"
	"encoding/json"

	"gopkg.in/yaml.v2"
	//"github.com/prometheus/procfs"
)

type Config struct {
	Log string `yaml:"Log"`
	ScriptDir string `yaml:"ScriptDir"`
	Init string `yaml:"Init"`
	Services []string `yaml:"Services"`
	Commands []string `yaml:"Commands"`
	Scripts []string `yaml:"Scripts"`
}
var cfg Config

func parseConfig(cfg *Config, file string) {
	f, err := os.Open(file)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)
	if err != nil {
		log.Fatalln(err)
	}
}

func setupLogging() {
	f, err := os.OpenFile(cfg.Log, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		log.Println(err)
	}
	log.SetOutput(f)
}

func inArray(k string, list []string) bool {
	for _, v := range list {
		if k == v {
			return true
		}
	}
	return false
}

func isConfigured(key string, ktype string) bool {
	if ktype == "service" {
		return inArray(key, cfg.Services)
	} else if ktype == "command" {
		return inArray(key, cfg.Commands)
	} else if ktype == "script" {
		return inArray(key, cfg.Scripts)
	} else {
		return false
	}
}

type status struct {
	Stdout string `json:"Stdout"`
	Stderr string `json:"Stderr"`
	Timestamp string `json:Timestamp`
}

func get_command(cmd string) *status {
	args := strings.Fields(cmd)
	com := exec.Command(args[0], args[1:]...)
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	com.Stdout = &stdout
	com.Stderr = &stderr
	com.Run()

	return &status{
		Stdout: stdout.String(),
		Stderr: stderr.String(),
		Timestamp: time.Now().String(),
	}
}

func get_service(service string) *status {
	if cfg.Init == "openrc" {
		return get_command("rc-service " + service + " status")
	} else if cfg.Init == "systemd" {
		return get_command("systemctl status " + service)
	} else {
		return &status{
			Stdout: cfg.Init + " is not a supported init type. Try openrc, or systemd.",
			Stderr: "Configuration error, cannot process get_service call.",
			Timestamp: time.Now().String(),
		}
	}
		
}

func get_script(script string) *status {
	com := exec.Command(cfg.ScriptDir + script)
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	com.Stdout = &stdout
	com.Stderr = &stderr
	com.Run()

	return &status{
		Stdout: stdout.String(),
		Stderr: stderr.String(),
		Timestamp: time.Now().String(),
	}
}

func auskultanto(w http.ResponseWriter, r *http.Request) {
	log.Println("Queried: " + r.URL.Path)

	if r.URL.Path == "/service" {
		name, ok := r.URL.Query()["name"]
		log.Println("Service key: " + name[0])
		
		if !ok || len(name[0]) < 1 {
			log.Println("Service name not specified!")
			return
		} else if isConfigured(name[0], "service") == false {
			log.Println(name[0] + " is not a configured service.")
			return
		}
		status := get_service(string(name[0]))
		json.NewEncoder(w).Encode(status)
	} else if r.URL.Path == "/command" {
		name, ok := r.URL.Query()["name"]
		log.Println("Command key: " + name[0])
		
		if !ok || len(name[0]) < 1 {
			log.Println("Command name not specified!")
			return
		} else if isConfigured(name[0], "command") == false {
			log.Println(name[0] + " is not a configured command.")
			return
		}
		status := get_command(string(name[0]))
		json.NewEncoder(w).Encode(status)
	} else if r.URL.Path == "/script" {
		name, ok := r.URL.Query()["name"]
		log.Println("Script key: " + name[0])
		
		if !ok || len(name[0]) < 1 {
			log.Println("Script name not specified!")
			return
		} else if isConfigured(name[0], "script") == false {
			log.Println(name[0] + " is not a configured script.")
			return //change this to return json on err with a stat:err and msg: err msg
		}
		status := get_script(string(name[0]))
		json.NewEncoder(w).Encode(status)
	}
}

func handleReq() {
	http.HandleFunc("/", auskultanto)
	log.Fatal(http.ListenAndServe(":8092", nil))
}

func main() {
	if _, err := os.Stat("/etc/auskultanto/config.yaml"); !os.IsNotExist(err) {
		parseConfig(&cfg, "/etc/auskultanto/config.yaml")
	} else {
		parseConfig(&cfg, "config.yaml")
	}

	setupLogging()
	handleReq()
}
