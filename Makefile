BINDIR ?= /usr/bin

compile:
	cd src && go build

install-bin:
	install -Dm 755 src/auskultanto $(BINDIR)/auskultanto

install-directories:
	install -Dm 644 -o root -g root .src/config.yaml /etc/auskultanto/config.yaml
	install -d -Dm 755 -o auskultanto -g adm /var/auskultanto/scripts

install-openrc:
	install -Dm 755 ./init/openrc/auskultanto /etc/init.d/

install-systemd:
	install -Dm 755 ./init/systemd/auskultanto.service /etc/systemd/system/
