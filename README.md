# What?
Auskultanto - Esperanto: Listener
A configurable system check status reporter

## Build
```
make compile
```

## Install
```
make install
```

Creates the following user:
```
auskultanto
```

Installs the following directories:
```
rw-r-r		root:root			/etc/auskultanto/
rw-r-r		root:root			/etc/auskultanto/config.yaml
rwxr-xr-x	auskultanto:adm		/var/auskultanto/scripts
rw-rw-rw	auskultanto:adm 	/var/log/auskultanto.log
```

# License:
GPLv3
